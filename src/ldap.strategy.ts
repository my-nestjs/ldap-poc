import * as Strategy from 'passport-ldapauth';
import { PassportStrategy } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';
import { Request } from 'express';
import { ldapConstants } from './config/constants';

@Injectable()
export class LdapStrategy extends PassportStrategy(Strategy, 'ldap') {
  constructor() {
    super(
      {
        passReqToCallback: true,
        // server: {
        //   url: 'ldap://192.168.1.253:389',

        //   // AD Connection
        //   bindDN: 'CN="Administrator",CN=Users,DC=sirisoft,DC=local',
        //   bindCredentials: 'P@ssw0rd',

        //   // Search
        //   searchBase: 'dc=sirisoft,dc=local',
        //   searchFilter: '(cn={{username}})',
        // },
        server: {
          url: ldapConstants.url,

          // AD Connection
          bindDN: ldapConstants.bindDN,
          bindCredentials: ldapConstants.bindCredentials,

          // Search
          searchBase: 'dc=example,dc=com',
          searchFilter: '(uid={{username}})',
        },
      },
      async (req: Request, user: any, done) => {
        req.user = user;
        return done(null, user);
      },
    );
  }
}
