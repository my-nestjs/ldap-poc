import { Module } from '@nestjs/common';
import { WeightRubberController } from './weight-rubber.controller';

@Module({
  controllers: [WeightRubberController],
})
export class WeightRubberModule {}
