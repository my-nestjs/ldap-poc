import { Controller, Get, UseGuards } from '@nestjs/common';
import { WeightRubberGuard } from 'src/guards/weight-rubber.guard';

@Controller('weight-rubber')
export class WeightRubberController {
  @UseGuards(WeightRubberGuard)
  @Get('with-rubber-guard')
  getWeightRubberId() {
    return {
      weightId: '13',
    };
  }

  @Get('with-no-rubber-guard')
  getWeightRubberIdNoGuard() {
    return {
      weightId: '13',
    };
  }
}
