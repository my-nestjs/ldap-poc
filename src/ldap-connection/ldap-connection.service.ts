import { Injectable } from '@nestjs/common';
import * as ldap from 'ldapjs';
import { ldapConstants } from 'src/config/constants';

@Injectable()
export class LdapConnectionService {
  ldapConnected(): any {
    const opt = {
      url: ldapConstants.url,
      reconnect: true,
      bindDN: ldapConstants.bindDN,
      bindCredentials: ldapConstants.bindCredentials,
    };
    const ldapClient = ldap.createClient(opt);
    return ldapClient;
  }

  //   /*use this to search user, add your condition inside filter*/
  //   searchUser() {
  //     var opts = {
  //       //  filter: '(objectClass=*)',  //simple search ไม่ใส่ เป็น filter: 'Defalut'
  //       // filter: '(&(uid=nana)(sn=Na))',// and search
  //       // filter: '(|(uid=nana)(sn=Na)(cn=nana))', // or search
  //       // filter:'(mail=nana@example.com)' ,
  //       scope: 'sub',
  //       attributes: ['dn', 'displayName'],
  //     };

  //     this.connection.search(
  //       'cn=lulu,ou=people,dc=example,dc=com',
  //       { scope: 'sub', attributes: ['uid', 'dn', 'displayName'] },
  //       function (err, res) {
  //         console.log('search function');
  //         if (err) {
  //           console.log('Error in search ' + err);
  //         } else {
  //           res.on('searchEntry', function (entry) {
  //             console.log('entry: ' + JSON.stringify(entry.object));
  //             // console.log(entry.object);
  //           });
  //           res.on('searchReference', function (referral) {
  //             console.log('referral: ' + referral.uris.join());
  //           });
  //           res.on('error', function (err) {
  //             console.error('error: ' + err.message);
  //           });
  //           res.on('end', function (result) {
  //             console.log('status: ' + result.status);
  //           });
  //         }
  //       },
  //     );
  //   }
}
