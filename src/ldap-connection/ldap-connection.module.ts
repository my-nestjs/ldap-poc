import { Module } from '@nestjs/common';
import { LdapConnectionService } from './ldap-connection.service';

@Module({
  providers: [LdapConnectionService],
  exports: [LdapConnectionService],
})
export class LdapConnectionModule {}
