import { Test, TestingModule } from '@nestjs/testing';
import { PriviledgeService } from './priviledge.service';

describe('PriviledgeService', () => {
  let service: PriviledgeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [PriviledgeService],
    }).compile();

    service = module.get<PriviledgeService>(PriviledgeService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
