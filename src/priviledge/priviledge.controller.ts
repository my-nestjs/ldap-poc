import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Post,
  Query,
} from '@nestjs/common';
import { PriviledgeUserDto } from './priviledge-user.dto';
import { PriviledgeService } from './priviledge.service';

@Controller('/priviledge')
export class PriviledgeController {
  constructor(private priviledgeService: PriviledgeService) {}

  //super admin เพิ่ม priviledge ให้เจ้าหน้าที่
  @Post()
  async addPriviledge(@Body() body: PriviledgeUserDto) {
    const priviledge = await this.priviledgeService.addPriviledge(
      body.priviledge,
      body.uid,
      body.displayName,
      body.mail,
    );
    return {
      data: priviledge,
      status: HttpStatus.CREATED,
      message: 'เพิ่มสิทธิสำเร็จ',
    };
  }

  //super admin หา user หาโดย  uid ใน AD server
  @Get('search-ldap-user')
  async searchLdapUser(
    @Query('uid') uid: string,
    @Query('displayName') displayName: string,
  ) {
    const searchResult = await this.priviledgeService.searchUser(
      uid,
      displayName,
    );
    if (!searchResult) throw new NotFoundException(`not found user`);
    return {
      data: searchResult,
      status: HttpStatus.OK,
      message: 'ค้นหาสำเร็จ',
    };
  }
}
