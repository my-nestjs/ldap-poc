import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { LdapConnectionService } from 'src/ldap-connection/ldap-connection.service';
import { Priviledge } from './priviledge.entity';
import { Connection } from 'typeorm';

@Injectable()
export class PriviledgeService {
  constructor(
    @InjectRepository(Priviledge)
    private repo: Repository<Priviledge>,
    private ldapConnection: LdapConnectionService,
    private connection: Connection,
  ) {}

  async addPriviledge(
    priviledge: string,
    uid: string,
    displayName: string,
    mail: string,
  ): Promise<any> {
    const ldapUser = await this.searchLdapUserByUid(uid);
    console.log(ldapUser);
    if (!ldapUser)
      throw new NotFoundException(`have no userid: ${uid} in LDAP server`);

    const priviledgeUser = this.repo.create({
      priviledge,
      uid,
      displayName,
      mail,
    });
    const user = await this.findUserPriviledge(uid);
    if (user) throw new BadRequestException('user already have priviledge');
    // const priviledgeUser = this.connection.query(
    //   `INSERT INTO public.priviledge(
    //     priviledge, uid, "displayName", mail)
    //     VALUES ('${priviledge}', '${uid}', '${displayName}', '${mail}')`,
    // );
    // console.log(priviledgeUser);

    return this.repo.save(priviledgeUser);
    // return priviled;
  }

  //หาสิทธิ
  async findUserPriviledge(uid: string): Promise<any> {
    const priviledge = await this.repo.findOne({
      uid: uid,
    });
    // if (!priviledge) throw new NotFoundException();
    return priviledge;
  }

  async searchUser(uid: string, displayName: string): Promise<any> {
    let data;
    return new Promise((resolve, reject) => {
      this.ldapConnection.ldapConnected().search(
        'dc=example,dc=com',
        {
          filter: `(|(uid=${uid})(displayName=${displayName}))`,
          // filter: `(uid=*)`,
          scope: 'sub',
          attributes: ['uid', 'dn', 'displayName', 'mail'],
        },
        async (err, res) => {
          res.on('searchEntry', (entry) => {
            data = entry.object;
          });
          res.on('error', (err) => {
            reject(err);
          });
          res.on('end', (result) => {
            resolve(data);
          });
        },
      );
    });
  }

  async searchLdapUserByUid(uid: string): Promise<any> {
    let data;
    return new Promise((resolve, reject) => {
      this.ldapConnection.ldapConnected().search(
        'dc=example,dc=com',
        {
          filter: `(uid=${uid})`,
          // filter: `(uid=*)`,
          scope: 'sub',
          attributes: ['uid', 'dn', 'displayName', 'mail'],
        },
        async (err, res) => {
          res.on('searchEntry', (entry) => {
            data = entry.object;
          });
          res.on('error', (err) => {
            reject(err);
          });
          res.on('end', (result) => {
            resolve(data);
          });
        },
      );
    });
  }
}
