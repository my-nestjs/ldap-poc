import { Test, TestingModule } from '@nestjs/testing';
import { PriviledgeController } from './priviledge.controller';

describe('PriviledgeController', () => {
  let controller: PriviledgeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [PriviledgeController],
    }).compile();

    controller = module.get<PriviledgeController>(PriviledgeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
