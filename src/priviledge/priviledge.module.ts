import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { LdapConnectionService } from 'src/ldap-connection/ldap-connection.service';
import { PriviledgeController } from './priviledge.controller';
import { Priviledge } from './priviledge.entity';
import { PriviledgeService } from './priviledge.service';

@Module({
  imports: [TypeOrmModule.forFeature([Priviledge])],
  controllers: [PriviledgeController],
  providers: [PriviledgeService, LdapConnectionService],
  exports: [PriviledgeService],
})
export class PriviledgeModule {}
