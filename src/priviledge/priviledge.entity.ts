import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Priviledge {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  priviledge: string;

  @Column()
  uid: string;

  @Column()
  displayName: string;

  @Column()
  mail: string;
}
