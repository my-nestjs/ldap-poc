import { IsString } from 'class-validator';

export class PriviledgeUserDto {
  @IsString()
  priviledge: string;
  @IsString()
  uid: string;
  @IsString()
  displayName: string;
  @IsString()
  mail: string;
}
