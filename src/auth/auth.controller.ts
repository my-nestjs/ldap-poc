import { Controller, HttpStatus, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { JwtAuthGuard } from 'src/guards/jwt-auth.guard';
import { RegisterRubberGuard } from 'src/guards/register-rubber.guard';
import { PriviledgeUserDto } from 'src/priviledge/priviledge-user.dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(JwtAuthGuard)
  @UseGuards(AuthGuard('ldap'))
  @Post('/login')
  async login(@Req() req: any) {
    const user = await this.authService.login(req.user);

    return {
      data: {
        uid: req.user,
        displayName: user.displayName,
        mail: user.mail,
      },
      status: HttpStatus.OK,
      message: 'เข้าสู่ระบบสำเร็จ',
      accessToken: user.access_token,
    };
  }
}
