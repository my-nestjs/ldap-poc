import { BadRequestException, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { PriviledgeService } from 'src/priviledge/priviledge.service';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private priviledge: PriviledgeService,
  ) {}

  async login(uid: string) {
    //เช็คสิทธิใน db
    const priviledge = await this.priviledge.findUserPriviledge(uid);
    if (!priviledge) throw new BadRequestException('Not have any priviledge');

    const payload = {
      priviledge: priviledge.priviledge,
      uid: priviledge.uid,
    };

    return {
      access_token: this.jwtService.sign(payload),
      displayName: priviledge.displayName,
      mail: priviledge.mail,
    };
  }

  //use in auth.middleware
  verifyJwt(jwt: string): Promise<any> {
    return this.jwtService.verifyAsync(jwt);
  }
}
