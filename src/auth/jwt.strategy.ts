import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { jwtConstants } from '../config/constants';
import { Strategy } from 'passport-local';
import { ExtractJwt } from 'passport-jwt';
import { PriviledgeService } from 'src/priviledge/priviledge.service';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy, 'jwt') {
  constructor(private priviledgeService: PriviledgeService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: jwtConstants.secret,
    });
  }

  async validate(uid: string) {
    const user = await this.priviledgeService.searchLdapUserByUid(uid);
    if (!user) {
      throw new UnauthorizedException();
    }
    return uid;
  }
}
