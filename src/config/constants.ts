export const postgresConstants = {
  type: 'postgres',
  host: '127.0.0.1',
  port: 5432,
  username: 'postgres',
  password: 'mysecretpassword',
  database: 'permission_db',
  synchronize: true,
};

export const ldapConstants = {
  url: 'ldap://68.183.224.29:389',

  // AD Connection
  bindDN: 'cn=admin,dc=example,dc=com',
  bindCredentials: '123456',
};

export const jwtConstants = {
  secret: 'secret',
  expiresIn: '1800s',
};
