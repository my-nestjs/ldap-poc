import { CanActivate, ExecutionContext } from '@nestjs/common';

export class WeightRubberGuard implements CanActivate {
  canActivate(context: ExecutionContext) {
    const request = context.switchToHttp().getRequest();
    return request.user.priviledge == 'weightRubber';
  }
}
