import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { LdapStrategy } from './ldap.strategy';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { LdapConnectionModule } from './ldap-connection/ldap-connection.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PriviledgeModule } from './priviledge/priviledge.module';
import { Priviledge } from './priviledge/priviledge.entity';
import { jwtConstants, postgresConstants } from './config/constants';
import { RegisterRubberModule } from './register-rubber/register-rubber.module';
import { JwtStrategy } from './auth/jwt.strategy';
import { AuthMiddleware } from './middlewares/auth.middleware';
import { WeightRubberModule } from './weight-rubber/weight-rubber.module';
import { JwtModule } from '@nestjs/jwt';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      envFilePath: `.env.${process.env.NODE_ENV}`,
    }),
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: postgresConstants.host,
      port: postgresConstants.port,
      username: postgresConstants.username,
      password: postgresConstants.password,
      database: postgresConstants.database,
      entities: [Priviledge],
      synchronize: true,
    }),
    AuthModule,
    LdapConnectionModule,
    PriviledgeModule,
    RegisterRubberModule,
    WeightRubberModule,
  ],
  controllers: [AppController],
  providers: [AppService, LdapStrategy, JwtStrategy],
  exports: [],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude(
        { path: '/priviledge', method: RequestMethod.POST },
        { path: '/priviledge/search-ldap-user', method: RequestMethod.GET },
        { path: '/auth/login', method: RequestMethod.POST },
      )
      .forRoutes('');
  }
}
