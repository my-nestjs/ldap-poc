import { Module } from '@nestjs/common';
import { RegisterRubberController } from './register-rubber.controller';

@Module({
  controllers: [RegisterRubberController],
})
export class RegisterRubberModule {}
