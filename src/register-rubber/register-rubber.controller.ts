import { Controller, Get, UseGuards } from '@nestjs/common';
import { RegisterRubberGuard } from 'src/guards/register-rubber.guard';

@Controller('register-rubber')
export class RegisterRubberController {
  @UseGuards(RegisterRubberGuard)
  @Get('with-rubber-guard')
  getRegisterRubberId() {
    return {
      registerId: '13',
    };
  }

  @Get('with-no-rubber-guard')
  getRegisterRubberIdNoGuard() {
    return {
      registerId: '13',
    };
  }
}
